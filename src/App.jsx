import React, { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroller";
import "./App.scss";
import axios from "axios";
import { InputBase, AppBar, Typography, Modal } from "@material-ui/core";
import SentimentVeryDissatisfiedIcon from "@material-ui/icons/SentimentVeryDissatisfied";
import ImageHolder from "./components/ImageHolder/ImageHolder";
import ImageOverlay from "./components/ImageOverlay/ImageOverlay";

function App() {
  const [searchTerms, setSearchTerms] = useState("");
  const [oldSearchTerms, setOldSearchTerms] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [foundPhotos, setFoundPhotos] = useState([]);
  const [firstLoad, setFirstLoad] = useState(true);
  const [loading, setLoading] = useState(false);
  const [overlayToggled, setOverlayToggled] = useState(false);
  const [activePhoto, setActivePhoto] = useState({});

  const buildPhotosArray = (existingPhotos, newPhotos) => {
    newPhotos.photo.forEach((photo) => {
      existingPhotos.push(photo);
    });

    return existingPhotos;
  };

  const postRequestCommonActions = (photos) => {
    setLoading(false);
    setFoundPhotos(photos);
  };

  useEffect(() => {
    setLoading(true);

    if (searchTerms.length === 0) {
      axios
        .get(
          `https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=abe2adc8194dda29bd40047b7545362b&format=json&nojsoncallback=1&safe_search=1&per_page=12&page=${currentPage}&text=ba747`
        )
        .then((data) => {
          const { photos } = data.data;
          const currentPhotos = foundPhotos;
          const photosToShow = buildPhotosArray(currentPhotos, photos);
          postRequestCommonActions(photosToShow);
          setFirstLoad(false);
        });
    }

    const delayInputSearch = setTimeout(() => {
      if (searchTerms.length > 0) {
        axios
          .get(
            `https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=abe2adc8194dda29bd40047b7545362b&format=json&nojsoncallback=1&safe_search=1&per_page=12&page=${currentPage}&text=${searchTerms}`
          )
          .then((data) => {
            const { photos } = data.data;
            let currentPhotos = [];
            if (oldSearchTerms === searchTerms) {
              currentPhotos = foundPhotos;
            }
            console.log(currentPhotos)
            const photosToShow = buildPhotosArray(currentPhotos, photos);
            postRequestCommonActions(photosToShow);
            setOldSearchTerms(searchTerms);

          });
      }
    }, 1000);

    return () => clearTimeout(delayInputSearch);
  }, [searchTerms, currentPage, firstLoad]);

  const fetchMoreData = (next) => {
    const { innerWidth: width } = window;
    if (!loading) {
      if (width > 767) {
        setCurrentPage(next);
      }
    }
  };

  const toggleOverlay = (photoDetails, photoInfo) => {
    setOverlayToggled(true);
    setActivePhoto({
      photoDetails,
      photoInfo,
    });
  };

  return (
    <div>
      <AppBar className="searchBar">
        <InputBase
          placeholder="Search…"
          onChange={(e) => setSearchTerms(e.target.value)}
          inputProps={{ "aria-label": "search" }}
        />
      </AppBar>

      {overlayToggled && (
        <Modal
          style={{ padding: "3em" }}
          open={overlayToggled}
          onClose={() => setOverlayToggled(false)}
        >
          <ImageOverlay photo={activePhoto} />
        </Modal>
      )}

      <div className={`App ${loading ? "loading" : ""}`}>
        {foundPhotos.length > 0 && (
          <InfiniteScroll
            pageStart={0}
            hasMore
            loadMore={() => {
              fetchMoreData(currentPage + 1);
            }}
            threshold={300}
          >
            {foundPhotos.map((foundPhoto, index) => {
              return (
                <ImageHolder
                  photo={foundPhoto}
                  key={`${foundPhoto.id}-${foundPhoto.secret}-${index}`}
                  toggleOverlay={toggleOverlay}
                />
              );
            })}
          </InfiniteScroll>
        )}

        {foundPhotos.length === 0 && (
          <Typography variant="h2" className="error-msg">
            <SentimentVeryDissatisfiedIcon fontSize="large" /> No Photos Found
          </Typography>
        )}
      </div>
    </div>
  );
}

export default App;
