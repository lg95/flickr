import React, { useEffect, useState } from "react";
import "./ImageHolder.scss";
import { Card, CardMedia, CardHeader, CardContent, Typography, Button, CardActions } from "@material-ui/core";
import axios from "axios";
import PropTypes from "prop-types";

const ImageHolder = ({ photo, toggleOverlay }) => {
  const { photo: photoInfo } = photo;
  const [photoDetails, setPhotoDetails] = useState();
  const [width, setWidth] = useState(window.innerWidth);
  const toggledClass = "collapsed";
  let tagString = "";

  useEffect(() => {
    axios
      .request({
        url: `https://www.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=abe2adc8194dda29bd40047b7545362b&format=json&nojsoncallback=1&safe_search=1&secret=${photo.secret}&photo_id=${photo.id}`,
      })
      .then((data) => {
        const { photo: retrievedPhotoInfo } = data.data;
        setPhotoDetails(retrievedPhotoInfo);
      });

    const handleResize = () => {
      setWidth(window.innerWidth);
    };

    window.addEventListener("resize", handleResize);
  }, [photo.id, photo.secret]);

  photoDetails?.tags.tag.forEach((t) => {
    tagString += `${t.raw}, `;
  });

  return (
    <Card className="pictureTile">
        <CardHeader className="pictureTile__header" title={photoDetails?.title._content || "Untitled"}/>
        <CardMedia className="pictureTile__picture" image={`https://live.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg`}/>
        <CardContent className="pictureTile__descriptions">
          <Typography>
            <a
              href={`https://www.flickr.com/photos/${photoDetails?.owner.nsid}/${photo.id}`}
            >
              {photoDetails?.title._content || "Untitled"}
            </a>
            by
            <a
              href={`https://www.flickr.com/photos/${photoDetails?.owner.nsid}/`}
            >
              {photoDetails?.owner.username}
            </a>
          </Typography>
          <Typography
            className={`pictureTile__text--description ${toggledClass}`}
          >
            {photoDetails?.description._content || "No description provided"}
          </Typography>
          <Typography
            className={`pictureTile__text--description ${toggledClass}`}
          >
            <span style={{ fontWeight: "bold" }}>Tags </span>
            {tagString || "No tags set"}
          </Typography>
        </CardContent>
      <CardActions className="pictureTile__text">
        
        {width > 1200 && (
          <Button
            onClick={() => toggleOverlay(photoInfo, photoDetails)}
            variant="outlined"
          >
            More info
          </Button>
        )}
      </CardActions>
    </Card>
  );
};

export default ImageHolder;

ImageHolder.propTypes = {
  photo: PropTypes.object.isRequired,
  toggleOverlay: PropTypes.func.isRequired,
};
