import React, { useEffect, useState } from "react";
import "./ImageOverlay.scss";
import {
  Typography,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Card,
  CardContent,
  CardMedia
} from "@material-ui/core";
import PropTypes from "prop-types";

import axios from "axios";

const ImageOverlay = React.forwardRef(({ photo }, ref) => {
  const [deatiledStats, setDetailedStats] = useState({});
  useEffect(() => {
    if (!deatiledStats.camera) {
      axios
        .request({
          url: `https://www.flickr.com/services/rest/?method=flickr.photos.getExif&api_key=abe2adc8194dda29bd40047b7545362b&format=json&nojsoncallback=1&photo_id=${photo.photoInfo.id}`,
        })
        .then((data) => {
          const { photo: retrievedPhotoDetails } = data.data;

          if (retrievedPhotoDetails && retrievedPhotoDetails.exif) {
            retrievedPhotoDetails.exif.forEach((e) => {
              if (e.label === "Lens") {
                retrievedPhotoDetails.lens = e.raw._content;
              }
            });
            setDetailedStats(retrievedPhotoDetails);
          }
        });
    }
  });

  return (
    <Card tabIndex="-1" className="imageOverlay" ref={ref}>
      <div className="imageOverlay__details">
        <CardContent className="imageOverlay__content">
          <TableContainer>
              <Table aria-label="simple table">
                <TableBody>
                  <TableRow>
                    <TableCell align="right">Image Format</TableCell>
                    <TableCell align="right">
                      {photo.photoInfo.originalformat || "Not Provided"}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="right">Camera Used</TableCell>
                    <TableCell align="right">
                      {deatiledStats.camera || "Not Provided"}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell align="right">Lens Used</TableCell>
                    <TableCell align="right">
                      {deatiledStats.lens || "Not Provided"}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
        </CardContent>
      </div>
      <CardMedia
        className="imageOverlay__image"
        image={`https://live.staticflickr.com/${photo.photoInfo.server}/${photo.photoInfo.id}_${photo.photoInfo.secret}.jpg`}
      />
    </Card>
  );
});

export default ImageOverlay;

ImageOverlay.propTypes = {
  photo: PropTypes.object.isRequired,
};
